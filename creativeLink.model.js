const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CreativeLinkSchema = new Schema({
    url: String,
    code: {type: String, unique: true},
    clicks: Number
});

module.exports = mongoose.model('CreativeLink', CreativeLinkSchema, 'creativeLinks');
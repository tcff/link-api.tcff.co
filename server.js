require("dotenv").config();

const PORT = process.env.PORT || 80;
const MONGO_URL = process.env.MONGO_URL;
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require("mongoose");

const app = express();
const CreativeLink = require("./creativeLink.model");

mongoose.connect(MONGO_URL, { useNewUrlParser: true }).then(() => {
    console.log('mongodb connected');
}).catch((error) => {
    console.log(error);
});

mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", (req, res) => {
    res.send("<div style='text-align: center'><h1>Welcome To The Creative Link API</h1></div>")
});

app.get("/link/:code", (req, res) => {
    CreativeLink.findOne({code:req.params.code}).exec((error, doc) => {
        if (error) {
            res.status(505).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: "something failed",
                data: error
            });
        }
        else {
            if (!doc) {
                res.status(401).json({
                    statusCode: 401,
                    timestamp: Date.now(),
                    message: "document missing"
                });
            }
            else {
                res.status(202).json({
                    statusCode: 202,
                    timestamp: Date.now(),
                    message: "operation passed",
                    data: doc
                });
            }
        }
    });
});

app.post("/link/new", (req, res) => {
    const generateCode = (length) => {
      let c = "";
      for (let x = 0; x < length; x++) {
        c += Math.floor(Math.random() * length);
      }
      return "g"+c
    };

    const randomNumber = Math.floor(Math.random() * 6);
    const code = generateCode(randomNumber);

    CreativeLink({
        url: req.body.url,
        code: code,
        clicks: 0
    }).save((error, creativeLink) => {
        if (error) {
            res.status(505).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: "something failed",
                data: error
            });
        }
        else {
            res.status(202).json({
                statusCode: 202,
                timestamp: Date.now(),
                message: "operation passed",
                data: creativeLink
            });
        }
    });
});

// this method is to customize a link and change the code it has
// thats why this method has two operations, check if the code requested exists, and then replace

app.post("/link/:code", (req, res) => {
    const newCode = req.body.newCode;
    CreativeLink.findOne({code: newCode}).exec((error, docOne) => {
        if (error) {
            res.status(505).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: "something failed",
                data: error
            });
        }
        else {
            if (!docOne) {
                CreativeLink.findOne({code: req.params.code}).exec((error, docTwo) => {
                    if (error) {
                        res.status(505).json({
                            statusCode: 505,
                            timestamp: Date.now(),
                            message: "something failed",
                            data: error
                        });
                    }
                    else {
                        if (!docTwo) {
                            res.status(401).json({
                                statusCode: 401,
                                timestamp: Date.now(),
                                message: "document missing"
                            });
                        }
                        else {
                            CreativeLink.findOneAndUpdate({code: code}, {code: newCode}, (error, creativeLink) => {
                                if (error) {
                                    res.status(505).json({
                                        statusCode: 505,
                                        timestamp: Date.now(),
                                        message: "something failed",
                                        data: error
                                    });
                                }
                                else {
                                    res.status(202).json({
                                        statusCode: 202,
                                        timestamp: Date.now(),
                                        message: "operation passed",
                                        data: {
                                            oldCode: req.params.code,
                                            newCode: creativeLink.code
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
            else {
                res.status(301).json({
                    statusCode: 301,
                    timestamp: Date.now(),
                    message: "Code taken"
                });
            }
        }
    });
});


// this method is used for g.tcff.co and redirects a user using the shortened link to
// the destination attatched to the link

app.get("/:code", (req, res) => {
    const code = req.params.code;

    CreativeLink.findOne({code: code}).exec((error, creativeLink) => {
        if (error) {
            res.send("<div style='text-align: center;'><h1>Something went wrong... </h1></div>");
        }
        else {
            if (!creativeLink) {
                res.send("<div style='text-align: center;'><h1>Hmmm... This Creative Link Doesnt Exist</h1></div>");
            }
            else {
                res.redirect(creativeLink.url);
            }
        }
    });
});

app.listen(PORT, () => console.log("The Creative Link API on port: "+PORT));
